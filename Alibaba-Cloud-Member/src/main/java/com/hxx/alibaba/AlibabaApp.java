package com.hxx.alibaba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author huangxiangxiang
 * @version 2.0.0
 * @createTime 2019年08月19日 16:15:00
 */

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class AlibabaApp {
    public static void main(String[] args) {
        SpringApplication.run(AlibabaApp.class, args);
    }
}
